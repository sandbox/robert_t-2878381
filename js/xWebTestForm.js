(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.netforum = {
    attach: function (context, settings) {

      $('#edit-netforum-request').on('change', function () {
        updateParameterForm();
      });

      updateParameterForm();
    }
  }

  function updateParameterForm() {
    $('#parameters').hide("fast");
    if (getParamsForSelectedFunction() == "parameters") {
      $.getJSON(document.location.pathname + "/params_for/" + getSelectedFunction(), function (json) {
        var fctn = getSelectedFunction();
        drupalSettings.netforum.xwebFunctions[fctn] = json;
        updateParameters(getParamsForSelectedFunction());
        $('#parameters').show("normal");
      });
    }
    else {
      updateParameters(getParamsForSelectedFunction());
      $('#parameters').show("normal");
    }
  };

  function getParamsForSelectedFunction() {
    var fctn = getSelectedFunction();
    if (drupalSettings.netforum.xwebFunctions[fctn].toString() == "parameters") {
      return "parameters";
    }
    return drupalSettings.netforum.xwebFunctions[fctn];
  };

  function getSelectedFunction() {
    return $('#edit-netforum-request').val();
  };

  function updateParameters(parameter_list) {
    var paramsDiv = $('#parameters div.fieldset-wrapper');
    paramsDiv.empty();
    if (parameter_list.length == 0) {
      paramsDiv.html("<i>no parameters</i>");
    }
    else {
      addParamsInto(paramsDiv[0], parameter_list);
    }
  };

  function addParamsInto(paramsDiv, parameter_list) {
    if (typeof(drupalSettings.netforum.paramId) == "undefined") {
      drupalSettings.netforum.paramId = 0;
    }
    if (typeof(drupalSettings.netforum.objStack) == "undefined") {
      drupalSettings.netforum.objStack = [];
    }

    jQuery.each(parameter_list, function (key, val) {
      if (typeof(val) == "string") {
        drupalSettings.netforum.paramId += 1;
        paramsDiv.appendChild(buildInputDiv(drupalSettings.netforum.paramId, val, drupalSettings.netforum.objStack));
      }
      else if (typeof(val) == "object") {
        drupalSettings.netforum.objStack.push(key);
        var legend = document.createElement('legend');

        var legendSpan = document.createElement('span');
        legendSpan.setAttribute('class', 'fieldset-legend');
        legendSpan.innerHTML = key;
        legend.appendChild(legendSpan);

        var newFieldset = document.createElement('fieldset');
        newFieldset.setAttribute('class', 'collapsible');
        newFieldset.setAttribute('title', key);
        newFieldset.setAttribute('id', 'fieldset-' + drupalSettings.netforum.paramId);
        newFieldset.appendChild(legend);

        var wrapper = document.createElement('div');
        wrapper.setAttribute('class', 'fieldset-wrapper');
        addParamsInto(wrapper, val);

        newFieldset.appendChild(wrapper);
        paramsDiv.appendChild(newFieldset);
        drupalSettings.netforum.objStack.pop();
      }
    });
  };

  function buildInputDiv(pnum, pname, stack) {
    var prefix = "";
    if (stack.length > 0) {
      prefix = "[" + stack.join("][") + "]";
    }

    var newDiv = document.createElement('div');
    newDiv.setAttribute('id', 'netforum_custom_' + pnum);
    newDiv.setAttribute('class', 'form-item');

    var newLabel = document.createElement('label');
    newLabel.setAttribute('for', 'edit-netforum-param-' + pnum);
    newLabel.innerHTML = pname + ":";
    newDiv.appendChild(newLabel);

    var newInput = document.createElement('input');
    newInput.setAttribute('type', 'text');
    newInput.setAttribute('maxlength', '2000');
    newInput.setAttribute('name', 'netforum_params' + prefix + "[" + pname + "]");
    newInput.setAttribute('id', 'edit-netforum-param-' + pnum);
    newInput.setAttribute('size', 60);
    newInput.setAttribute('class', 'form-text');

    // When we are redisplaying a form, this var will be populated.  We work through
    // the stack and walk through the vars to set the defaults
    var form_defaults = drupalSettings.netforum.formDefaults;
    if (typeof(form_defaults) != "undefined" && form_defaults[getSelectedFunction()]) {
      var defaults = form_defaults[getSelectedFunction()];
      if (typeof(defaults[pname]) != "undefined") {
        newInput.value = defaults[pname];
      }
    }
    newDiv.appendChild(newInput);
    return newDiv;
  };

})(jQuery, Drupal, drupalSettings);

