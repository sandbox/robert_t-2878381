(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.netforum = {
    attach: function (context, settings) {

      var actionUrl = drupalSettings.netforum.cacheClearUrl;

      $(".netforum_clear_button").on("click", function(event) {
        event.preventDefault();
        var request = $(this).attr("id").replace("button_clear_", "");
        $.get(
          actionUrl + "/" + request + "/clear",
          function(data) {
            var mssg = data + " cache " + (data == 1 ? "record has" : "records have") + " been deleted."
            $("#" + request + "_message").text(mssg);
            window.setTimeout(function() {
              $("#" + request + "_message").text(" ");
            }, 3000);
            updateCount(request, 'active', data);
          }
        );
      });

      $(".netforum_purge_button").on("click", function(event) {
        event.preventDefault();
        var request = $(this).attr("id").replace("button_purge_", "");
        $.get(
          actionUrl + "/" + request + "/purge",
          function(data) {
            var mssg = data + " expired cache " + (data == 1 ? "record has" : "records have") + " been deleted."
            $("#" + request + "_message").text(mssg);
            window.setTimeout(function() {
              $("#" + request + "_message").text(" ");
            }, 3000);
            updateCount(request, 'expired', data);
          }
        );
      });

    }
  }

  function updateCount(request, which, removed) {
    var selector = "#" + request + "_" + which;
    var currentVal = $(selector).text();
    var newVal = currentVal - removed;
    if (newVal < 0 ) {
      newVal = 0;
    }
    $(selector).text(newVal);
    updateButtons(request);
    if (which == "active") {
      updateCount(request, "expired", removed);
    }
  }

  function updateButtons(request) {
    var active = $("#" + request + "_active").text();
    var expired = $("#" + request + "_expired").text();
    if (expired == 0) {
      $("#button_purge_" + request).addClass("is-disabled").prop("disabled",true);
      if (active == 0) {
        $("#button_clear_" + request).addClass("is-disabled").prop("disabled",true);
      }
    }
  }

})(jQuery, Drupal, drupalSettings);
