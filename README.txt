README.txt - netFORUM

INTRODUCTION
netFORUM is an association management system built by Avectra, which is now part
of Abila. netFORUM comes in two flavors:
  - netFORUM Enterprise, a single-company SaaS solution
  - netFORUM Pro, a multi-tenant SaaS solution.
See http://www.abila.com for details.

The netFORUM module is desiged to provide a common platform for retrieving data
from and pushing data to netFORUM via xWeb.

xWeb is the remote service part of the netFORUM product  -- the others being
iWeb (the admin GUI) and eWeb (the member portal) -- and is an XML over SOAP
service. xWeb is well documented on the Avectra wiki at 
http://wiki.avectra.com/NetFORUM (login may be required).


THIS MODULE
This version of the netforum module is a rewrite for Drupal 8 based on the
Drupal 7 version of this module. There are some minor changes and enhancements
from the D7 version.

xWeb requests were cached in a local table in the D7 version. The D8 version
uses the same request caching mechanism, but with admin selection of whether to
cache and the cache duration on a per-request basis. The cache table is trimmed
on cron runs and flushed on Drupal cache rebuild.

This module was tested against netFORUM Enterprise build 2015.1.5.


REQUIREMENTS
PHP with SOAP and SimpleXML. Check for SimpleXML and SOAP by examining the
output from phpinfo();


IIS NOTES
The netFORUM only works with xWeb Secure and does not implement HTTP Auth
security. xWeb Secure should be running without directory security on IIS (at
least not passwords)

Create the xWeb user like adding a regular user, but using SQL manager set the
usr_pwd. For example:
UPDATE fw_user SET usr_pwd = 'testXwebPassword' WHERE usr_code = 'DrupalxWebUser'

Sometimes PHP does not get along nicely with Microsofts IIS when using HTTPS for
communication; this is described at
http://us2.php.net/manual/en/wrappers.http.php.
The netFORUM module hides all errors and warnings when making requests in favor
of throwing its own.


CONFIGURATION
Read more about how to set up and configure netforum client at
admin/help/netforum

The default timeout for a request to xWeb is set by the default_socket_timeout
setting in php.ini, which is usually 60 seconds.  That's a long time to make a
user wait, so there is functionality for verifying that xWeb is avaiable before
requests. If you wish to disable that, changing the default_socket_timeout is
recommended.

PHP caches the SOAP WSDL, which for us is a good thing. The WSDL describes all
of the functions available for us. The WSDL is unlikely to change, so a high
value is recommended. If xWeb is unavailable then best attempts are made to
return values, but if the WSDL is unavailable then no attempt can be made. That
means that if xWeb is unavailable for longer than the WSDL is cached for, you
will run into problems. Set this in the php.ini file, look for the
soap.wsdl_cache_ttl option.

The netforum_object_cache table is configured with longtext columns to store the
results which should be PLENTY of room,but by default MySQL will only allow
packets up to 1MB in size. If you use queries that return large amounts of data
be sure to increase the max_allowed_packet size. You may need to add a line to
your my.cnf file, something like:
  max_allowed_packet = 10M


DEVELOPING:
The code is all documented using the Drupal standards and the Drupal API module
can provide them for you.  There are some helper functions that will create XML
suitable for Insert and Update operations from arrays. If you are having
problems make sure that the case of your request is correct. GetFacadeXMLSchema
is correct, GetFacadeXmlSchema will return nothing. Check the logs to see if you
are getting no response to your queries, and if so test it at
/admin/config/netforum/xwebtest.

The key function and most likely to be used is netforum_xweb_request(), and
netforum_is_empty_guid() is pretty handy too.


HISTORICAL NOTES
From the Drupal 6 version:
Drupal release
James Michael-Hill
james.michael-hill@utc.org
The Utilities Telecom Council

xWebSecureClient Class - an extension of the php SoapClient class to provide for
seamless sliding token based authentication with Avectra's netFORUM.  xWeb
documentation is provided with your system and at http://demo.avectra.com/xWebHelp/
Read more about netFORUM at www.avectra.com

Developed under php 5.1.6/apache 2.  PHP 5 is REQUIRED.

Where possible functionality is parallel to the regular SoapClient with the 
exception of the constructor and the __soapCall() method - which will not take
any soap headers.

The constructor requires xwebUserName and xwebUserPass to be set.
These can be your regular user name and password, but that is NOT RECOMMENDED.
See the Avectra documentation for best practices, but in a nutshell there should
be a dedicated user, and the password is set in the usr_pwd field of the fw_user
table.  For more examples see the included xwebExample.php file.

Drupal Release:
This release is based on the January 24th, 2007 release and intended for use
solely with the drupal CMS system. Changes have been made to the database
caching routines to work with the drupal framework.  If you're interested in
using this class for other purposes please contact me or look for the official
xWebSecureClient class.

