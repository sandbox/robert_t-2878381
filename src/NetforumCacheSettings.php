<?php
/**
 * @file
 * Contains Drupal\netforum\NetforumCacheSettings
 */

namespace Drupal\netforum;

/**
 * Defines a utility class for accessing the cache settings for the netforum
 * module.
 */
class NetforumCacheSettings {

  /**
   * The config object.
   */
  protected $config;


  /**
   * Build a new NetForumCacheSettings object.
   */
  public function __construct() {
    $this->config = \Drupal::config('netforum.settings');
  }

  /**
   * Retrieve all cache settings records.
   *
   * @return array
   *   An array of rows of cache settings records.
   */
  public function allCacheSettings() {
    return $this->config->get('cache_requests');
    
  }

  /**
   * Retrieve the cache settings records where caching is enabled.
   *
   * @return array
   *   An array of rows of cache settings records.
   */
  public function cachedRequestDurations() {
    $cached = [];
    $settings = $this->allCacheSettings();
    foreach ($settings as $request => $setting) {
      if ($setting->cache) {
        $cached[$request] = $setting->duration;
      }
    }
    return $cached;
  }

  /**
   * Retrieve a list of cached requests.
   *
   * @return array
   *   An array of request names where caching is enabled.
   */
  public function allCachedRequestNames() {
    $names = [];
    $settings = $this->allCacheSettings();
    foreach ($settings as $request => $setting) {
      if ($setting['cache']) {
        $names[] = $request;
      }
    }
    return $names;
  }

  /**
   * Retrieve a complete cache setting record for one request.
   *
   * @param string $request
   *   The netFORUM xWeb request. Examples: 'GetQuery', 'ExecuteMethod'.
   *
   * @return array
   *   An array with keys:
   *    - request: the string parameter.
   *    - cache_flag: boolean flag for this request. 1 = cache, 0 = no cache.
   *    - cache_duration: a string suitable for use with strtotime().
   *   The array with keys will always be returned. If a match is not found,
   *   the values will be empty strings.
   */
  public function cacheSetting($request) {
    $obj = $this->config->get("cache_requests.$request");
    $empty = empty($obj->request);
    return [
      'request'        => $empty ? '' : $obj->request,
      'cache_flag'     => $empty ? '' : $obj->cache,
      'cache_duration' => $empty ? '' : $obj->duration,
    ];
  }

  /**
   * Retrieve the cache duration value, as an integer, from a cache setting
   * record.
   *
   * This function translates the duration string into an integer number of
   * seconds a cache record is valid.
   *
   * @param string $request
   *   The netFORUM xWeb request. Examples: 'GetQuery', 'ExecuteMethod'.
   *
   * @return int
   *   The number of seconds until cache expiration.
   */
  public function cacheDurationTime($request) {
    $duration = $this->config->get("cache_requests.$request.duration");
    $now = time();
    return strtotime("+$duration", $now) - $now;
  }

  /**
   * Retrieve the cache duration value, as a string, from a cache setting
   * record.
   *
   * @param string $request
   *   The netFORUM xWeb request. Examples: 'GetQuery', 'ExecuteMethod'.
   *
   * @return string
   *   The cache duration string suitable for use with strtotime().
   */
  public function cacheDurationString($request) {
    return $this->config->get("cache_requests.$request.duration");
  }

  /**
   * Check if the request is set to be cached.
   * @param string $request
   *   The netFORUM xWeb request. Examples: 'GetQuery', 'ExecuteMethod'.
   *
   * @return bool
   *   1 = can be or is being cached, 0 = 'no cache' setting.
   */
  public function canCacheRequest($request) {
    return $this->config->get("cache_requests.$request.cache");
  }

}
