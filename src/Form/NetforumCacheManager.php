<?php
/**
 * @file
 * Contains Drupal\netforum\Form\NetforumCacheManager
 */

namespace Drupal\netforum\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Link;
use Drupal\Core\Url;

use Drupal\netforum\NetforumCacheSettings;
use Drupal\netforum\NetforumDbStorage;

/**
 * Implements the NetforumXwebTestForm.
 */
class NetforumCacheManager extends FormBase {

  /**
   * The netforum database storage class.
   *
   * @var \Drupal\netforum\NetforumDbStorage
   */
  protected $dbStorage;

  /**
   * The netforum settings class.
   *
   * @var \Drupal\netforum\NetforumCacheSettings
   */
  protected $settings;


  /**
   * Create a new NetforumCacheManager object.
   */
  public function __construct(NetforumDbStorage $db_storage, NetforumCacheSettings $cache_settings) {
    $this->dbStorage = $db_storage;
    $this->settings = $cache_settings;
  }

  /**
   * Override the parent create() method.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('netforum.db_storage'),
      $container->get('netforum.cache_settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'netforum_cache_manager';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'netforum/netforum.cache_manager';
    $url = Url::fromRoute('netforum.cache_clear')->toString();
    $form['#attached']['drupalSettings']['netforum']['cacheClearUrl'] = $url;

    $form['message_area'] = [
      '#type' => 'markup',
      '#markup' => '&nbsp;',
      '#prefix' => '<div id="netforum_message">',
      '#suffix' => '</div>',
    ];

    $url = Url::fromRoute('netforum.settings');
    $link = Link::fromTextAndUrl($this->t('xWeb Settings'), $url)->toRenderable();
    $markup = $this->t('xWeb requests may be cached in the database to ' . 
      'reduce the number of repetitive xWeb calls. Whether a request type is ' .
      'cached, and for how long the cached record can be used is set on the ' .
      '@link tab.', [
        '@link' => render($link),
      ]);

    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => $markup,
      '#prefix' => '<div id="netforum_intro_text">',
      '#suffix' => '</div>',
    ];

    $form['buttons'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'netforum_buttons_wrapper',
      ],
    ];

    $form['buttons']['clear'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear all request caches'),
      '#attributes' => [
        'id' => 'netforum_clear_all',
        'class' => ['netforum_button', ],
      ],
    ];
    $form['buttons']['purge'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purge expired cached requests'),
      '#attributes' => [
        'id' => 'netforum_purge_all',
        'class' => ['netforum_button', ],
      ],
    ];

    $form['table'] = [
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('Request'), 'class' => ['col1']],
        ['data' => $this->t('# of active records'), 'class' => ['col2']],
        ['data' => $this->t('# of expired records'), 'class' => ['col3']],
        ['data' => $this->t('Actions'), 'class' => ['col4']],
        ['data' => '', 'class' => ['col5']],
      ],
      '#caption' => $this->t('Cache Records by Request'),
      '#empty' => $this->t('There are no request cache records.'),
      '#attributes' => [
        'id' => 'netforum_cache_table',
      ],
    ];
    $cache_settings = $this->settings->allCacheSettings();
    $in_cache = $this->dbStorage->getRequestCacheSummary();
    $request_array = [];
    foreach($cache_settings as $request => $cache_setting) {
      $request_array[] = $request;
      $form['table'][$request]['#attributes'] = [
        'class' => ['row ', "row_$request", $request, ],
      ];
      $form['table'][$request]['request'] = [
        '#type' => 'markup',
        '#plain_text' => $request,
        '#wrapper_attributes' => ['class' => ['col1']]
      ];

      if (empty($in_cache[$request])) {
        $row = [
          'active' => 0,
          'expired' => 0,
          'cache' => $cache_setting['cache'],
          'disabled' => TRUE,
        ];
        $in_cache[$request] = [];
      }
      else {
        $row = $this->prepareRowData($cache_setting, $in_cache[$request]);
      }

      $form['table'][$request]['active'] = [
        '#type' => 'markup',
        '#markup' => $row['cache'] ? $row['active'] : '---',
        '#wrapper_attributes' => [
          'id' => $request . '_active',
          'class' => ['col2'],
        ],
      ];
      $form['table'][$request]['expired'] = [
        '#type' => 'markup',
        '#markup' => $row['cache'] ? $row['expired'] : '---',
        '#wrapper_attributes' => [
          'id' => $request . '_expired',
          'class' => ['col3'],
        ],
      ];
      $form['table'][$request]['action'] = [
        '#type' => 'container',
        '#wrapper_attributes' => [
          'class' => ['col4'],
        ],
      ];
      $form['table'][$request]['action']['clear'] = [
        '#type' => 'button',
        '#value' => $this->t('Clear all'),
        '#attributes' => [
          'id' => "button_clear_$request",
          'class' => ['netforum_button', 'netforum_request_button netforum_clear_button', ],
        ],
        '#disabled' => $row['disabled'],
      ];
      $form['table'][$request]['action']['purge'] = [
        '#type' => 'button',
        '#value' => $this->t('Purge expired'),
        '#attributes' => [
          'id' => "button_purge_$request",
          'class' => ['netforum_button', 'netforum_request_button netforum_purge_button', ],
        ],
        '#disabled' => $row['disabled'] || empty($row['expired']),
      ];
      $form['table'][$request]['message'] = [
        '#type' => 'markup',
        '#markup' => '',
        '#wrapper_attributes' => [
          'id' => $request . '_message',
          'class' => ['col5'],
        ],
      ];
    }
    $form['#attached']['drupalSettings']['netforum']['requests'] = $request_array;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $op = $form_state->getValue('op');
    if ($op && $op == $form['buttons']['clear']['#value']) {
      drupal_set_message($this->t('@number cache records have been deleted.', [
        '@number' => 7,
      ]));
    }
    elseif ($op && $op == $form['buttons']['purge']['#value']) {
      drupal_set_message($this->t('@number cache records have been deleted.', [
        '@number' => $this->dbStorage->pruneRequestCache(),
      ]));
    }
    $form_state->setRebuild();
  }

  protected function prepareRowData(array $cache_setting, array $in_cache) {
    $row = [
      'active' => 0,
      'expired' => 0,
      'cache' => $cache_setting['cache'],
      'disabled' => TRUE,
    ];
    if ($cache_setting['cache'] && !empty($in_cache)) {
      $row['disabled'] = FALSE;
      $active = 0;
      $expired = 0;
      $duration = $cache_setting['duration'];
      $expire_time = strtotime("-$duration");
      foreach ($in_cache as $timestamp) {
        if ($timestamp >= $expire_time) {
          $row['active'] += 1;
        }
        else {
          $row['expired'] += 1;
        }
      }
    }
    return $row;
  }

}
