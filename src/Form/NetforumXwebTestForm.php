<?php
/**
 * @file
 * Contains Drupal\netforum\Form\NetforumXwebTestForm
 */

namespace Drupal\netforum\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;


/**
 * Implements the NetforumXwebTestForm.
 */
class NetforumXwebTestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'netforum_xweb_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request_code = '';
    $response = NULL;

    // Attach the JavaScript and CSS files. The library also includes jQuery and
    // drupalSettings. This will allow us to add JavaScript variables.
    $form['#attached']['library'][] = 'netforum/netforum.xwebtest';

    // First, get the functions as an array with function names as keys and an
    // array of function parameters as the value.
    $xweb_functions = [];
    $function_list = netforum_xweb_soap_functions();
    foreach (array_keys($function_list) as $function_name) {
      $xweb_functions[$function_name] = netforum_xweb_function_parameters($function_name);
    }

    if (isset($_POST['netforum_request'])) {
      // Enable form caching to prevent double queries of netFORUM.
      $form_state->setCached();

      if (isset($_POST['netforum_params']) && is_array($_POST['netforum_params'])) {
        $request_parameters = netforum_array_filter_recursive($_POST['netforum_params']);
      }
      else {
        $request_parameters = [];
      }
      $form['#attached']['drupalSettings']['netforum']['formDefaults'] = [
        $_POST['netforum_request'] => $request_parameters,
      ];


      $request_code .= "\$arguments = " . var_export($request_parameters, TRUE) . ";\n";
      $request_code .= "\$response = netforum_xweb_request('" . $_POST['netforum_request'] . "', \$arguments); ";

      $response = netforum_xweb_request($_POST['netforum_request'], $request_parameters);

      // Overwrite the existing set of parameters (that did shallow inspection)
      // with a deep inspection of the current function.
      $xweb_functions[$_POST['netforum_request']] = netforum_xweb_function_struct_parameters($_POST['netforum_request']);
    }

    // Turn our array of functions and parameters to json for later use.
    $form['#attached']['drupalSettings']['netforum']['xwebFunctions'] = $xweb_functions;

  
    $request_options = netforum_xweb_functions();
    if (count($request_options) == 0) {
      $this->messenger()->addError('No netFORUM xWeb functions found. Perhaps' .
        ' xWeb is currently unavailable?');
    }

    $request_log = netforum_request_log();
    $request_soap_log = netforum_soap_log();
    
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Test a request here by selecting the function ' .
        'from the drop down list and filling in the parameters below. The ' .
        'result of the request and debugging information will be shown below.'),
    ];

    $form['netforum_xweb_request'] = [
      '#title' => 'xWeb Request',
      '#type' => 'fieldset',
      '#description' => 'Construct the xWeb request',
    ];

    $form['netforum_xweb_request']['netforum_request'] = [
      '#type' => 'select',
      '#title' => t('netFORUM xWeb Function'),
      '#description' => t('The name of the function to call'),
      '#default_value' => $form_state->hasValue('netforum_request') ? $form_state->getValue('netforum_request') : 'GetQuery',
      '#options' => $request_options,
    ];

    $form['netforum_xweb_request']['parameters'] = [
      '#type' => 'fieldset',
      '#attributes' => ['id' => 'parameters'],
      '#description' => 'Parameters for this method',
      '#title' => 'Parameters',
    ];

    // Normally, there would be inputs for the function here, but we're relying
    // on some JS and AJAX to build the right form for the right function.

    $form['netforum_xweb_request']['submit'] = [
      '#type' => 'button',
      '#value' => 'submit',
      '#name' => 'clickbutton',
    ];

    $form['netforum_xweb_results'] = [
      '#title' => 'xWeb Results',
      '#type' => 'fieldset',
      '#description' => 'Response from xWeb',
    ];

    $form['netforum_xweb_results']['code'] = [
      '#title' => 'PHP Code',
      '#type' => 'item',
      '#description' => "<pre>" . htmlspecialchars($request_code) . "</pre>",
    ];

    $form['netforum_xweb_results']['response'] = [
      '#title' => 'Response',
      '#type' => 'item',
      '#description' => "<pre>" . htmlspecialchars(print_r($response, TRUE)) . "</pre>",
    ];

    $form['netforum_xweb_results']['soap_log'] = [
      '#title' => 'SOAP Log',
      '#type' => 'item',
      '#description' => "<pre>" . htmlspecialchars($request_soap_log) . "</pre>",
    ];

    $form['netforum_xweb_results']['request_log'] = [
      '#title' => 'xWebSecureClient Class Log',
      '#type' => 'item',
      '#description' => "<pre>" . htmlspecialchars($request_log) . "</pre>",
    ];
    /*
    $form['#submit'] = [
      'netforum_test_page' => [],
    ];
    */
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}