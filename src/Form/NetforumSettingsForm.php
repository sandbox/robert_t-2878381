<?php
/**
 * @file
 * Contains Drupal\netforum\Form\NetforumSettingsForm
 */

namespace Drupal\netforum\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Configure netforum xWeb interface settings.
 */
class NetforumSettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'netforum_settings';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['netforum.settings'];
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('netforum.settings');

    // Add the css file. It's in a library defined in netforum.libraries.yml.
    $form['#attached']['library'][] = 'netforum/netforum.settings';

    $form['xweb'] = [
      '#type' => 'details',
      '#title' => $this->t('xWeb Connection'),
      '#open' => TRUE,
      '#weight' => 0,
    ];
    $form['xweb']['wsdl_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('netFORUM WSDL URL'),
      '#description' => $this->t('xWeb endpoint, commonly ' .
        'https://example.com/xweb/Secure/netFORUMXML.asmx?WSDL.'),
      '#default_value' => $config->get('wsdl_url'),
      '#required' => TRUE,
      '#size' => 50,
    ];
    $form['xweb']['xweb_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('xWeb user name'),
      '#description' => $this->t('This is the username that is used to ' .
        'authenticate with netFORUM.'),
      '#default_value' => $config->get('xweb_username'),
      '#size' => 16,
    ];
    if (empty($config->get('xweb_password'))) {
      $description = $this->t('The password to go with the xweb user.');
    }
    else {
      $description = $this->t('You already have a password set. You do not ' .
        'need to enter a password unless you wish to change it.');
    }
    $form['xweb']['xweb_password'] = [
      '#type' => 'password',
      '#title' => t('xWeb user password'),
      '#description' => $description,
      '#default_value' => $config->get('xweb_password'),
      '#size' => 16,
    ];
    $form['xweb']['enable_http_keepalive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable HTTP Keep-alive'),
      '#description' => $this->t('Enables using the same TCP connection for ' .
        'multiple SOAP requests. This reduces latency and is recommended to ' .
        'be enabled.'),
      '#default_value' => $config->get('enable_http_keepalive'),
    ];
    $form['xweb']['enable_http_compression'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable HTTP Compression'),
      '#description' => $this->t('Enables using HTTP Compression with GZIP ' .
        'encoding to increase transfer speeds of requests and responses. ' .
        'This improves performance and is recommended to be enabled.'),
      '#default_value' => $config->get('enable_http_compression'),
    ];
    $form['xweb']['allow_invalid_ssl'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow HTTPS invalid certificate'),
      '#description' => $this->t('Accept invalid or self-signed certificates ' .
        'for HTTPS connections. This setting should NOT be enabled in ' .
        'production environments.'),
      '#default_value' => $config->get('allow_invalid_ssl'),
    ];
    $form['xweb']['hide_error_messages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide connection error messages'),
      '#description' => $this->t('Should regular visitors be able to see ' .
        'when the connection to netFORUM is not available? Site ' .
        'administrators will always see connection errors.'),
      '#default_value' => $config->get('hide_error_messages'),
    ];


    $form['avail'] = [
      '#type' => 'details',
      '#title' => $this->t('xWeb availability'),
      '#open' => TRUE,
      '#weight' => 2,
    ];
    $form['avail']['verify_availability'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verify xWeb availability before requests'),
      '#description' => $this->t('Verify that netFORUM is available before ' .
        'issuing requests by trying to open a connection to the server.'),
      '#default_value' => $config->get('verify_availability'),
    ];
    $form['avail']['verify_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('xWeb verification length'),
      '#description' => $this->t('This sets how frequently netFORUM should ' .
        'be checked for availability. Think of it as the time between ' .
        'verification checks. <br>There is overhead associated with this, so ' .
        'anything less than 2 minutes might not be efficient.<br>Enter zero ' .
        'to disable and check before every request.<br> The default is `3`' .
        ' minutes.'),
      '#field_suffix' => $this->t('minutes'),
      '#default_value' => $config->get('verify_length'),
      '#size' => 4,
    ];
    $form['avail']['slow_query_limit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slow query limit'),
      '#description' => $this->t('Any xWeb requests over this time limit are ' .
        'marked with warnings in the system logs. The default is `1.5` seconds.'),
      '#default_value' => $config->get('slow_query_limit'),
      '#field_suffix' => $this->t('seconds'),
      '#size' => 4,
    ];
    $form['avail']['verify_timeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('xWeb timeout'),
      '#description' => $this->t('If netFORUM cannot be contacted in this ' .
        'time it is logged as an error. The default is `30` seconds.'),
      '#field_suffix' => $this->t('seconds'),
      '#default_value' => $config->get('verify_timeout'),
      '#size' => 4,
    ];


    $form['xWebProxy'] = [
      '#title' => "xWeb Proxy",
      '#type' => 'details',
      '#description' => $this->t('The following settings are for a HTTP ' .
        'proxy server to be used in front of the xWeb endpoint specified ' .
        'above. You can use this feature to aid development by monitoring ' .
        'all xWeb requests and responses being made.'),
        '#open' => FALSE,
        '#weight' => 4,
    ];
    $form['xWebProxy']['proxy_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use a HTTP Proxy Server'),
      '#description' => $this->t('Enable sending all xWeb requests through ' .
        'the proxy server specified below.'),
      '#default_value' => $config->get('web_proxy.proxy_enabled'),
    ];
    $form['xWebProxy']['proxy_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy Hostname'),
      '#description' => $this->t('The hostname of the proxy server to use.'),
     '#default_value' => $config->get('web_proxy.proxy_host'),
      '#size' => 50,
      '#maxlength' => 255,
    ];
    $form['xWebProxy']['proxy_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy Port'),
      '#description' => $this->t('The port of the proxy server to use.'),
      '#default_value' => $config->get('web_proxy.proxy_port'),
      '#size' => 16,
      '#maxlength' => 5,
    ];
    $form['xWebProxy']['proxy_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy Username'),
      '#description' => $this->t('Optional. Username if your proxy server ' .
        'requires authentication.'),
      '#default_value' => $config->get('web_proxy.proxy_user'),
      '#size' => 16,
      '#maxlength' => 255,
    ];
    $form['xWebProxy']['proxy_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Proxy Password'),
      '#description' => $this->t('Optional. Password if your proxy server ' .
        'requires authentication.'),
      '#default_value' => $config->get('web_proxy.proxy_password'),
      '#size' => 16,
      '#maxlength' => 255,
    ];


    $form['cache'] = [
      '#type' => 'details',
      '#title' => $this->t('xWeb Caching'),
      '#open' => TRUE,
      '#weight' => 8,
    ];

    // Get the table portion of the cache settings form.
    $this->requestCacheTable($form['cache']);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('netforum.settings');

    $clear_to_connect = TRUE;

    $wsdl_url = UrlHelper::filterBadProtocol(trim($form_state->getValue('wsdl_url')));
    $username = trim($form_state->getValue('xweb_username'));
    $password = trim($form_state->getValue('xweb_password'));

    if (!UrlHelper::isValid($wsdl_url, TRUE)) {
      $form_state->setErrorByName('netforum_wsdl_url', $this->t('Please enter a valid wsdl url for access.'));
      $clear_to_connect = FALSE;
    }
    if (empty($username)) {
      $form_state->setErrorByName('xweb_username', $this->t('Please enter a username.'));
      $clear_to_connect = FALSE;
    }
    if (empty($password)) {
      $old_password = $config->get('xweb_password');
      if ($old_password) {
        $form_state->setValue('xweb_password', $old_password);
        $password = $old_password;
      }
      else {
        $form_state->setErrorByName('xweb_password', $this->t('Please enter a password.'));
        $clear_to_connect = FALSE;
      }
    }
    if (empty($form_state->getValue('slow_query_limit'))) {
      $form_state->setErrorByName('slow_query_limit', $this->t('Please set a slow query limit. The default value is 1.5 seconds.'));
    }
    $verify_timeout = $form_state->getValue('verify_timeout');
    if (empty($verify_timeout)) {
      $form_state->setErrorByName('verify_timeout', $this->t('Please set the xWeb timeout. The default value is 5 seconds.'));
    }

    if (!empty($form_state->getValue('proxy_enabled')) && $form_state->getValue('proxy_enabled') == 1) {
      if (empty(trim($form_state->getValue('proxy_host')))) {
        $form_state->setErrorByName('proxy_host', $this->t('Please enter a proxy server host or disable use proxy server setting.'));
        $clear_to_connect = FALSE;
      }
      if (empty(trim($form_state->getValue('proxy_port')))) {
        $form_state->setErrorByName('proxy_port', $this->t('Please enter a proxy server port or disable use proxy server setting.'));
        $clear_to_connect = FALSE;
      }
    }


    $table = $config->get('cache_requests');
    foreach ($table as $request => $settings) {
      $duration = $settings['duration'];
      if (empty($duration)) {
        $form_state->setErrorByName('table][' . $request . '][duration', $this->t('Please set a cache duration for :request. An example is `1 hour`.', [
          ':request' => $request,
        ]));
      }
      elseif (substr(trim($duration), 0, 1) == '-') {
        $form_state->setValue($request, ['duration' => abs($duration)]);
      }
      elseif (strtotime('+' . $duration) === FALSE) {
        $form_state->setErrorByName('table][' . $request . '][duration', $this->t('The cache duration was not recognized for :request. Please enter a phrase like `30 minutes` or `2 hours`.',[
          ':request' => $request,
        ]));
      }
    }

    // xWeb On-Demand? On-Demand is TRUE; Enterprise is FALSE.
    // This variable is set here rather than in the submit() function so that it
    // can be used to test the connection to netFORUM.
    $this->config('netforum.settings')
      ->set('netforum_xweb_od', (stristr($wsdl_url, 'netForumXMLOnDemand.asmx') !== FALSE))
      ->save();

    // If no errors were triggered in the xWeb credentials section, attempt to
    // connect with the netFORUM server using the user-provided credentials.
    if ($clear_to_connect) {
      if ($form_state->getValue('verify_availability') == 1 && !netforum_is_available()) {
        $this->messenger()->addError('Could not contact the netFORUM server.');
        return;
      }
      try {
        if ($form_state->getValue('verify_length') != 0) {
          ini_set('default_socket_timeout', $verify_timeout);
        }

        $client_init_vars = [
          'trace' => TRUE,
          'exceptions' => TRUE,
          'xwebUserName' => $username,
          'xwebUserPass' => $password,
          'connection_timeout' => 20,
          'cache_wsdl' => WSDL_CACHE_BOTH,
        ];

        // Is HTTP keepalive or HTTP compression enabled?
        $client_init_vars['keep_alive'] = FALSE;
        if ($form_state->getValue('enable_http_keepalive') == 1) {
          $client_init_vars['keep_alive'] = TRUE;
        }
        if ($form_state->getValue('enable_http_compression') == 1) {
          $client_init_vars['compression'] = SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | 2;
        }

        // Allow invalid certificates?
        if ($form_state->getValue('allow_invalid_ssl') == 1) {
          $client_init_vars['stream_context'] = stream_context_create([
            'ssl' => [
              'verify_peer' => FALSE,
              'verify_peer_name' => FALSE,
              'allow_self_signed' => TRUE,
            ],
          ]);
        }

        // Is there a proxy server?
        if ($form_state->getValue('proxy_enabled') == 1) {
          $client_init_vars['proxy_host'] = $form_state->getValue('proxy_host');
          $client_init_vars['proxy_port'] = $form_state->getValue('proxy_port');
          if (!empty($form_state->getValue('proxy_user'))) {
            $client_init_vars['proxy_login'] = $form_state->getValue('proxy_user');
          }
          if (!empty($form_state->getValue('proxy_pass'))) {
            $client_init_vars['proxy_password'] = $form_state->getValue('proxy_pass');
          }
        }

        if (netforum_is_enterprise()) {
          $nfh = new \Drupal\netforum\xwebSecureClient($wsdl_url, $client_init_vars);
        }
        else {
          $nfh = new \Drupal\netforum\xwebSecureODClient($wsdl_url, $client_init_vars);
        }
        $response = $nfh->GetFacadeXMLSchema(['szObjectName' => 'Customer']);
        if ($response) {
          $this->messenger()->addStatus('Successfully connected to the netFORUM server and retrieved data.');
        }
      }
      catch (Exception $exception) {
        $form_state->setErrorByName('form_token', $this->t('Could not connect to the netFORUM server with specified parameters.<br> @exception_message .',
          ['@exception_message' => $exception->getMessage()]) );
        $wsdl_url_guess = $this->guess_xweb_url($wsdl_url);
        if (strtolower($wsdl_url) != strtolower($wsdl_url_guess)) {
          $this->messenger()->addStatus($this->t('Perhaps the following WSDL endpoint would work better:<br> @wsdl_url_guess',
            ['@wsdl_url_guess' => $wsdl_url_guess]));
        }
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Clean up text inputs before saving.
    $wsdl_url = UrlHelper::filterBadProtocol(trim($form_state->getValue('wsdl_url')));
    $xweb_username = trim($form_state->getValue('xweb_username'));
    $xweb_password = trim($form_state->getValue('xweb_password'));

    $config = $this->config('netforum.settings');

    // xWeb connection section
    $config->set('wsdl_url', $wsdl_url);
    $config->set('xweb_username', $xweb_username);
    $config->set('xweb_password', $xweb_password);
    $config->set('enable_http_keepalive', $form_state->getValue('enable_http_keepalive'));
    $config->set('enable_http_compression', $form_state->getValue('enable_http_compression'));
    $config->set('allow_invalid_ssl', $form_state->getValue('allow_invalid_ssl'));
    $config->set('hide_error_messages', $form_state->getValue('hide_error_messages'));

    // xWeb availability section
    $config->set('slow_query_limit', $form_state->getValue('slow_query_limit'));
    $config->set('verify_timeout', $form_state->getValue('verify_timeout'));
    $config->set('verify_availability', $form_state->getValue('verify_availability'));
    $config->set('verify_length', $form_state->getValue('verify_length'));

    // Cache section
    //$config->set('cache_max', $form_state->getValue('cache_max'));
    $cache_requests = $form_state->getValue('table');
    foreach ($cache_requests as $request => $settings) {
      $config->set("cache_requests.$request.cache", $settings['cache']);
      $config->set("cache_requests.$request.duration", $settings['duration']);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Builds the table portion of the settings form.
   *
   * @param array $form
   *   The form that is being modified, passed by reference.
   *
   * @see self::buildForm()
   */
  protected function requestCacheTable(array &$form) {
    $form['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Request'),
        $this->t('Cache?'),
        $this->t('Cache Duration'),
      ],
      '#caption' => $this->t('Caching by Request'),
      '#empty' => $this->t('No request cache items are available.'),
    ];
    $cache_requests = $this->config('netforum.settings')->get('cache_requests');
    foreach($cache_requests as $request => $cache_setting) {
      $form['table'][$request]['#attributes'] = [
        'class' => ['row ', "row_$request", $request, ],
      ];
      $form['table'][$request]['request'] = [
        '#type' => 'markup',
        '#markup' => $request,
      ];
      $form['table'][$request]['cache'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Yes, cache'),
        '#default_value' => $cache_setting['cache'],
      ];
      $form['table'][$request]['duration'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Cache duration'),
        '#title_display' => 'invisible',
        '#default_value' => $cache_setting['duration'],
        '#size' => 16,
      ];
    }
  }

  /**
   * Guess the xWeb endpoint based on the input.
   *
   *  @param string $url
   *    The URL entered by the user for the WSDL file location.
   *
   * @return string
   *  A string with the best guess for a valid xWeb url
   *
   */
  protected function guess_xweb_url($url) {
    $guess = $url;
    $xweb_dir = '/xWeb/Secure/netFORUMXML.asmx?WSDL';

    $od_servers = array(
      '66.28.41.163',
      'netforum.avectra.com',
      'www.netforumondemand.com',
    );
    $od_cert_server = 'www.netforumondemand.com';

    $parts = parse_url($url);
    if (!isset($parts['scheme'])) {
      $guess = 'http://' . $url;
    }

    $parts = parse_url($guess);

    if (!isset($parts['path'])) {
      // If it is just a host entry
      $guess = $guess . $xweb_dir;
    }
    else {
      // If they copied and pasted a URL from some other part of netforum.
      $sites = array('iweb', 'aweb', 'eweb', 'mweb');
      foreach ($sites as $s) {
        $sitepos = stripos($parts['path'], '/' . $s);
        if ($sitepos !== FALSE) {
          $new_path = substr($parts['path'], 0, $sitepos);
          $new_path .= $xweb_dir;
          $guess = $parts['scheme'] . '://' . $parts['host'] . $new_path;
        }
      }
    }

    //If they copied part, but didn't put the xWeb bit on the end
    if (! stristr($guess, 'xweb')) {
      $guess = rtrim($guess, '/'); //removing trailing slash since we add our own
      $guess = $guess . $xweb_dir;
    }
    else { //If they included xWeb, but not the netFORUMXML.asmx bit
      $guess = rtrim($guess, '/'); //removing trailing slash since we add our own
      if (strtolower(substr($guess, -4)) == 'xweb') {
        $guess = $guess . "/Secure/netFORUMXML.asmx?WSDL";
      }
      else {
        if (strtolower(substr($guess, -6)) == 'secure') {
          $guess = $guess . "/netFORUMXML.asmx?WSDL";
        }
      }
    }

    // If they are not using xWeb secure
    if (stristr($guess, 'xweb/netFORUMXML.asmx')) {
      $guess = str_ireplace('xweb/netforumxml.asmx', 'xweb/secure/netFORUMXML.asmx', $guess);
    }

    //If they forgot the ?WSDL on the end
    if (strtolower(substr($guess, -4)) == 'asmx') {
      $guess = $guess . "?WSDL";
    }

    foreach ($od_servers as $server) {
      if (stristr($guess, $server)) {
        //If they are using netFORUM OD, remove the /secure/ folder since OD
        // doesn't use it, and change the name of the web service page.
        $guess = str_ireplace('secure/netFORUMXML.asmx?WSDL', 'netForumXMLOnDemand.asmx?WSDL', $guess);
        if (stristr($guess, 'netforumODSandBox') === FALSE) { //if it is a live server
          if (strtolower(substr($guess, 0, 5)) != 'https') {
            $guess = 'https' . substr($guess, 4); //make sure they are using https
          }
          if ($server != $od_cert_server) {
            $guess = str_ireplace($server, $od_cert_server, $guess);
          }
        }
      }
    }

    return $guess;
  }

}
