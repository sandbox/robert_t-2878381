<?php
/**
 * @file
 * Contains Drupal\netforum\Controller\NetforumController
 */

namespace Drupal\netforum\Controller;
use Drupal\Core\Controller\ControllerBase;

use Drupal\netforum\NetforumDbStorage;
use Drupal\netforum\NetforumCacheSettings;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Defines the menu callback class for the netforum module.
 */
class NetforumController extends ControllerBase {

  /**
   * The database connection for the netforum tables.
   *
   * @var \Drupal\netforum\NetforumDbStorage
   */
  protected $dbStorage;

  /**
   * The cache settings class for managing cache settings.
   *
   * @var \Drupal\netforum\NetforumCacheSettings
   */
  protected $settings;

  /**
   * Construct a new NetforumController.
   */
  public function __construct(NetforumDbStorage $service_db_storage, NetforumCacheSettings $service_cache_settings) {
    $this->dbStorage = $service_db_storage;
    $this->settings = $service_cache_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('netforum.db_storage'),
      $container->get('netforum.cache_settings')
    );
  }

  /**
   * Return the parameters for xWeb testing of a particular xWeb request.
   * This function is called via Ajax, so the output is json.
   *
   * @param string $xweb_request
   *   The xWeb request for which the parameters are requested. Examples:
   *   GetQuery, ExecuteMethod.
   *
   * @return json
   *   Json-encoded output returned for an Ajax request.
   *
   * @see netforum_xweb_function_struct_parameters() in .module file.
   */
  public function params_for($xweb_request) {
    $params = netforum_xweb_function_struct_parameters($xweb_request);
    return new JsonResponse($params);
  }

  /**
   * Autocomplete responder for netFORUM object names.
   * This function is called via Ajax, so the output is json.
   *
   * @param string $search
   *   The xWeb object name or object prefix. Examples:
   *   Individual, ind, Organization, org.
   *
   * @return json
   *   Json-encoded output returned for an Ajax request.
   */
  public function object_autocomplete(Request $request, $search) {
    if ($search = $request->query->get('q')) {
      $obj_names = $this->dbStorage->searchObjectCache($search);
      return new JsonResponse($obj_names);
    }
  }

  /**
   * Clear or purge all request caches or only for a specific request.
   *
   * This is called from Ajax and returns a json object.
   *
   * @param string $affects
   *   The scope of the cache clear or purge action. Examples: 'all' will affect
   *   all request cache items. 'GetQuery' would affect only GetQuery cache
   *   items.
   *
   * @param string $action
   *   The desired action. 'clear' will remove all cache items in the scope.
   *   'purge' will remove only those cached items which have expired. Any other
   *   string will be ignored.
   *
   * @return json
   *   Json-encoded count of affected records is returned for an Ajax request.
   */
  public function cache_clear(string $affects, string $action) {
    if ($affects == 'all') {
      if ($action == 'clear') {
        return new JsonResponse($this->dbStorage->clearRequestCache());
      }
      elseif ($action == 'purge') {
        return new JsonResponse($this->dbStorage->pruneRequestCache());
      }
    }
    else {
      if ($action == 'clear') {
        return new JsonResponse($this->dbStorage->clearOneRequestCache($affects));
      }
      elseif ($action == 'purge') {
        return new JsonResponse($this->dbStorage->pruneOneRequestCache($affects));
      }
    }
  }

  /**
   * Utility function: refresh the cached objects for team installations.
   */
  protected function updateTeamObjectCache() {
    // If netFORUM is team, the only allowed objects are Individual,
    // Organization, Address and Customer. Calling GetFacadeXMLSchema on any
    // other objects is not permitted. Since we know these structures, these
    // may be stored directly without a netFORUM call.
    // Individual
      $params = [
        'obj_name' => 'Individual', 
        'obj_key' => 'f41b6e06-299b-4022-be6f-0641ba87de59', 
        'obj_prefix' => 'ind', 
        'obj_defaultcolumns' => 'ind_last_name,ind_first_name', 
        'obj_defaultorderby' => 'ind_last_name,ind_first_name', 
        'data' => NULL, 
        'obj_key_field' => NULL
      ];
      $this->dbStorage->mergeObjectCacheItem($params);
      // Organization
      $params = [
        'obj_name' => 'Organization', 
        'obj_key' => '9c0100ce-f6c2-4b42-aff2-1c065f3734d9', 
        'obj_prefix' => 'org', 
        'obj_defaultcolumns' => 'org_name, org_acronym', 
        'obj_defaultorderby' => 'org_name', 
        'data' => NULL, 
        'obj_key_field' => NULL
      ];
      $this->dbStorage->mergeObjectCacheItem($params);
      // Address
      $params = [
        'obj_name' => 'Address',
        'obj_key' => '0574b4d5-853b-48a9-8b64-8b424b6658bd',
        'obj_prefix' => 'adr',
        'obj_defaultcolumns' => '*',
        'obj_defaultorderby' => 'adr_city',
        'data' => NULL,
        'obj_key_field' => NULL,
      ];
      $this->dbStorage->mergeObjectCacheItem($params);
      // Customer
      $params = [
        'obj_name' => 'Customer', 
        'obj_key' => '65f0eb73-0df7-4196-b2ff-11b61f10e532', 
        'obj_prefix' => 'cst', 
        'obj_defaultcolumns' => 'cst_type,cst_name_cp', 
        'obj_defaultorderby' => 'cst_sort_name_dn', 
        'data' => NULL, 
        'obj_key_field' => NULL
      ];
      $this->dbStorage->mergeObjectCacheItem($params);
  }

}