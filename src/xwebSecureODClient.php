<?php
/**
 * @file
 * Contains Drupal\netforum\xwebSecureODClient
 */

namespace Drupal\netforum;
use \SoapHeader;

class xwebSecureODClient extends \SoapClient {
  /**
   * The credentials used for connections to netFORUM.
   */
  private $userName;
  private $userPass;

  /**
   * The authentication token returned from netFORUM.
   * Note that this token is valid for only a limited time period, after which
   * a new token must be retrieved.
   */
  private $authToken;

  /**
   * The namespace of the SOAP header element.
   * @see http://php.net/manual/en/soapheader.soapheader.php.
   */
  private $xwebNamespace;

  /**
   * An array of function names found in the WSDL called through normal methods.
   */
  private $overloadedWsdlFunctions = [];
  
  /**
   * An array of function names not called through normal methods.
   */
  private $wsdlNonOverloadFunctions = ['Authenticate'];

  /**
   * The URL or URI used to retrieve the WSDL.
   */
  private $cacheWsdlLoc = '';

  /**
   * The cached functions list are those xWeb requests which are cached. These
   * are set in the administrative settings form.
   */
  private $cachedFunctions = [];

  /**
   * Log record, publicly accessible for debugging purposes. Not perfect, but
   * if something is going awry it gives some insight.
   */
  public $log = '';

  /**
   * Explicitly enable or disable caching. By default, caching is done on a
   * per-request basis per the settings.
   */
  private $cachingOn = TRUE;

  /**
   * Flag for whether the last response was written to cache.
   */
  private $cachedResponseWritten = FALSE;

  /**
   * Flag for whether the last response was read from cache.
   */
  private $responseFromCache = FALSE;


  /**
   * Constructs a new xwebSecureClient object.
   *
   * @param mixed $wsdl
   *   The URL or URI of the WSDL file.
   * @param array $options
   *   Optional parameters, including cache_wsdl and ssl_method.
   *   @see http://php.net/manual/en/soapclient.soapclient.php.
   */
  public function __construct($wsdl, $options = NULL) {
    if (isset($options['xwebUserName'], $options['xwebUserPass'])) {
      $this->userName = $options['xwebUserName'];
      $this->userPass = $options['xwebUserPass'];
      parent::__construct($wsdl, $options);

      $this->cacheWsdlLoc = $wsdl;
      $this->setOverloadedWsdlFunctions();
      $cache_settings = \Drupal::service('netforum.cache_settings');
      $this->cachedFunctions = $cache_settings->allCachedRequestNames();
    }
    else {
      //throw constructor error if we don't have the needed parameters
      throw new Exception('Invalid parameters in xwebSecureODClient ' . 
        'constructor: xwebUserName and xwebUserPass are required.');
    }
    $this->log .= 'Finished constructor.' . "\n";
  }

  /**
   * Overloaded version of the parent __call() method.
   *
   * The __call() method is executed for every method call in this class.
   * We're using it to wrap every wsdl function call in the xWeb
   * authentication scheme. This method in turn calls __soapCall().
   *
   * @param string $method
   *   The xWeb function being called. Examples: GetQuery, ExecuteMethod.
   * @param array $arguments
   *   An array of $arguments used in the xWeb function.
   *
   * @return object
   *   The xWeb request response.
   */
  public function __call($method, $arguments) {
    // We're only overloading the functions that the wsdl defines, so check to
    // see if it is in our list.
    if (in_array($method, $this->overloadedWsdlFunctions) && !in_array($method, $this->wsdlNonOverloadFunctions)) {
      //$this->log .= "Overloading the call to $method method. \n";
      return $this->__soapCall($method, $arguments);
    }
  }

  /**
   * Connect to netFORUM using the SOAP client and return the response.
   *
   * This method overloads the PHP __soapCall() method.
   *
   * @param string $fname
   *   The xWeb request.
   * @param array $arguments
   *   An array of arguments for the xWeb function call.
   *
   * @return object
   *   The xWeb request response.
   */
  function __soapCall($fname, $arguments, $options = [], $input_headers = [], &$output_headers = []) {
    $this->log .= "Beginning __SoapCall. Function name = $fname." . "\n";

    $response = $this->cacheRetrieve($fname, $arguments);
    if ($response) {
      $this->log .= "Returning cached response to $fname call. \n";
      return $response;
    }
    // We've received no cached response. Try to connect to netFORUM.
    try {
      $responseHeaders = '';
      $response = parent::__soapCall($fname, $arguments, NULL, $this->getAuthHeaders(), $responseHeaders);
      $this->authToken = $responseHeaders['AuthorizationToken']->Token;
      if (in_array($fname, $this->cachedFunctions)) {
        $this->log .= 'Caching response to soap call for future use.' . "\n";
        $this->cacheStore($fname, $arguments, $response);
      }
    }
    catch (SoapFault $exception) {
      // If it is a bad token try re-authenticating - but only once.
      if (stristr($exception->faultstring, 'Invalid Token Value')) {
        $this->log .= 'Caught exception with invalid token value. ' .
          'Re-authenticating and trying one more time.' . "\n";
        $this->authToken = '';
        try {
          $response = parent::__soapCall($fname, $arguments, NULL, $this->getAuthHeaders(), $responseHeaders);
          $this->authToken = $responseHeaders['AuthorizationToken']->Token;
          if (in_array($fname, $this->cachedFunctions)) {
            $this->log .= 'Caching response to soap call for future use.' . "\n";
            $this->cacheStore($fname, $arguments, $response);
          }
        }
        catch (SoapFault $exception) {
          $this->log .= "Caught exception in soap call to $fname again - bad authentication token. \n";
          throw $exception;
        }
      }
      else {
        $this->log .= "Caught exception in soap call to $fname. \n";
        // Reset the auth token since a bad request invalidates any previous
        // auth token.  This will save us a step if we try again.
        $this->authToken = '';
        throw $exception;
      }
    }

    return $response;
  }

  /**
   * Wrapper for SOAPClient's __doRequest() that adds logging.
   */
  public function __doRequest($request, $location, $action, $version, $one_way = 0) {
    $this->log .= 'Beginning __doRequest.' . "\n";
    $this->log .= "Params for __doRequest: \nRequest: $request \n" .
      "Location: $location \nAction: $action \nVersion: $version \n\n";
    return parent::__doRequest($request, $location, $action, $version);
  }

  /**
   * Empties the local object's log.
   */
  public function clearLog() {
    $this->log = '';
  }

  /**
   * Prevent the use of the request cache for either reading or writing.
   */
  public function disableCaching() {
    $this->cachingOn = FALSE;
    $this->log .= 'Caching is disabled for this request.' . "\n";
  }

  /**
   * Turn on caching for requests, both for reading and writing.
   *
   * Note that the deprecated expire_time parameter is not used. Cache
   * expiration is on a per-request basis, using the administrative settings.
   */
  public function setCaching($expire_time = '') {
    $this->cachingOn = TRUE;
    $this->log .= 'Caching is permitted for this request.' . "\n";
  }

  /**
   * Retrieve the flag indicating if the last response was written to cache.
   */
  public function cachedLastResponse() {
    return $this->cachedResponseWritten;
  }

  //If the last response came from the cache this will return true;
  /**
   * Retrieve the flag indicating the last reponse was read from cache.
   */
  public function lastResponseFromCache() {
    return $this->responseFromCache;
  }

  /**
   * Retrieve the authentication headers.
   *
   * @return class SoapHeader
   *   A SoapHeader object.
   *
   * @see http://php.net/manual/en/class.soapheader.php.
   */
  private function getAuthHeaders() {
    // If we don't have a saved auth token, get one.
    if ((!isset($this->authToken)) || empty(trim($this->authToken))) {
      $this->log .= "\n" . 'Fetching new authToken.' . "\n";
      $authReqParams = [
        'userName' => $this->userName,
        'password' => $this->userPass,
      ];
      $responseHeaders = '';
      try {
        // Run the parent soap call to get the authentication token.
        $response = parent::__soapCall('Authenticate', ['parameters' => $authReqParams], NULL, NULL, $responseHeaders);
        $this->authToken = $responseHeaders['AuthorizationToken']->Token;
        $this->xwebNamespace = $response->AuthenticateResult;
      }
      catch (SoapFault $exception) {
        throw $exception;
      }
    }

    // Return the header object.
    return new SoapHeader($this->xwebNamespace, 'AuthorizationToken', ['Token' => $this->authToken], TRUE);
  }

  /**
   * Set the list of xWeb functions that will be overloaded using the
   * magic __call() method.
   */
  private function setOverloadedWsdlFunctions() {
    $functions = parent::__getFunctions();
    foreach ($functions as $fname) {
      // Extract the actual function name for our uses.
      $start = strpos($fname, ' ');
      $end = strpos($fname, '(');
      // Append the name of the function to our internal list, which we check in
      // every __call().
      $this->overloadedWsdlFunctions[] = trim(substr($fname, $start, ($end - $start)));
    }
  }

  /**
   * Store a netFORUM function request in the request cache.
   *
   * @param string $fname
   *   The xWeb request name. Examples: GetQuery, ExecuteMethod
   * @param array $arguments
   *   The parameters used in the request.
   *  @param mixed $response
   *    The response to the xWeb query
   *
   * @return bool
   *   TRUE if the a cache entry was added, FALSE otherwise.
   */
  private function cacheStore($fname = '', $arguments = '', $response = '') {
    if (!$this->cachingOn) {
      $this->log .= 'Could not store response: caching disabled for this request.' . "\n";
      $this->cachedResponseWritten = FALSE;
      return FALSE;
    }
    if ($fname == '' || $arguments == '' || (!is_object($response) && $response == '')) {
      $this->log .= 'Could not store response: invalid parameters passed.' . "\n";
      $this->cachedResponseWritten = FALSE;
      return FALSE;
    }
    if (!in_array($fname, $this->cachedFunctions)) {
      $this->log .= "Could not store response, $fname is not on the list of cacheable functions. \n";
      $this->cachedResponseWritten = FALSE;
      return FALSE;
    }

    $params = [
      'user_name' => $this->userName,
      'wsdl_loc' => $this->cacheWsdlLoc,
      'request' => $fname,
      'arguments_sha1_hash' => sha1(serialize($arguments)),
      'response' => serialize($response),
    ];
    $db_storage = \Drupal::service('netforum.db_storage');
    $success = $db_storage->insertRequestCacheItem($params);
    if ($success) {
      $this->log .= "Cached call to $fname. \n";
      $this->cachedResponseWritten = TRUE;
      return TRUE;
    }
    else {
      $this->log .= "Failure: Tried and failed to cache call to $fname. \n";
      $this->cachedResponseWritten = FALSE;
      return FALSE;
    }
  }

  /**
   * Retrieve a netFORUM function request from the request cache.
   *
   * @param string $fname
   *   The xWeb request name. Examples: GetQuery, ExecuteMethod
   * @param array $arguments
   *
   * @return mixed
   *   FALSE if a record was not retrieved. If a record is found, the
   *   unserialized response is returned.
   */
  private function cacheRetrieve($fname = '', $arguments = '') {
    if (!$this->cachingOn) {
      $this->log .= 'Could not fetch response from cache: caching disabled for this request.' . "\n";
      $this->responseFromCache = FALSE;
      return FALSE;
    }
    if (empty($fname) || empty($arguments)) {
      $this->log .= 'Could not fetch response from cache: invalid parameters passed.' . "\n";
      $this->responseFromCache = FALSE;
      return FALSE;
    }
    if (!in_array($fname, $this->cachedFunctions)) {
      $this->log .= "Could not fetch response from cache: $fname is not on the list of cacheable functions. \n";
      $this->responseFromCache = FALSE;
      return FALSE;
    }

    $params = [
      'user_name' => $this->userName,
      'wsdl_loc' => $this->cacheWsdlLoc,
      'request' => $fname,
      'arguments_sha1_hash' => sha1(serialize($arguments)),
    ];
    $db_storage = \Drupal::service('netforum.db_storage');
    $return_array = $db_storage->getRequestCacheItem($params);
    if (!empty($return_array['response'])) {
      $this->log .= "Found cached response to $fname. \n";
      $this->responseFromCache = TRUE;
      return unserialize($return_array['response']);
    }
    else {
      $this->log .= "No cached response found for $fname in database. \n";
      $this->responseFromCache = FALSE;
      return FALSE;
    }
  }

}
