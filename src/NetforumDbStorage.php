<?php
/**
 * @file
 * Contains Drupal\netforum\NetforumDbStorage
 */

namespace Drupal\netforum;

use Drupal\Core\Database\Connection;
use Drupal\netforum\NetforumCacheSettings;

/**
 * Defines the database storage class for the netforum schema.
 *
 * This class serves as a database wrapper for the xWeb request cache and the
 * netFORUM object cache. These two tables are defined in the .install file.
 */
class NetforumDbStorage {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The netforum settings class.
   *
   * @var \Drupal\netforum\NetforumCacheSettings
   */
  protected $settings;

  /**
   * Constructs a MyPageDbLogic object. 
   *
   * @param \Drupal\Core\Database\Connection $database 
   *   The database connection. 
   */
  // The $database variable came to us from the service argument. 
  public function __construct(Connection $connection, NetforumCacheSettings $settings) {
    $this->connection = $connection;
    $this->settings = $settings;
  }

  /**
   * Retrieve a request cache item.
   *
   * Note that only current (un-expired) cache entries are returned.
   *
   * @param array $params
   *   An array with keys
   *    - user_name: the xWeb user name being used.
   *    - wsdl_loc: the location of the WSDL being used.
   *    - request: the netFORUM xWeb request. Example: 'GetQuery'.
   *    - arguments_sha1_hash: sha-1-encoded serialized request arguments.
   *        Created using
   *        <code>
   *          $arguments_sha1_hash = sha1(serialize($arguments));
   *        </code>
   *
   * @return array
   *   An array with keys:
   *    - request: the string parameter.
   *    - response: the serialized response from netFORUM.
   *    - timestamp: the Unix timestamp of the record.
   *   The array with keys will always be returned. If no match is found, or
   *   if only expired matches are found, the values will be empty strings.
   */
  public function getRequestCacheItem($params) {
    $query = $this->connection->select('netforum_request_cache', 'r');
    $query->fields('r', ['request', 'response', 'timestamp', ]);
    $query->condition('user_name', $params['user_name']);
    $query->condition('wsdl_loc', $params['wsdl_loc']);
    $query->condition('request', $params['request']);
    $query->condition('arguments_sha1_hash', $params['arguments_sha1_hash']);
    // Get the duration from the cache setting and create a condition.
    $duration = $this->settings->cacheDurationString($params['request']);
    $query->condition('timestamp', strtotime('-' . $duration), '>=');
    $executed = $query->execute();
    $obj = $executed->fetch(\PDO::FETCH_OBJ);
    $empty = empty($obj->request);
    return [
      'request'   => $empty ? '' : $obj->request,
      'response'  => $empty ? '' : $obj->response,
      'timestamp' => $empty ? '' : $obj->timestamp,
    ];
  }

  /**
   * Retrieve all request cache items in summary format.
   *
   * Note that all records are returned, including expired records.
   *
   * @return array
   *   An array of arrays, of the format
   *   [
   *      request1_name => [ timestamp1, timestamp2, timestamp3, ...],
   *      request2_name => [ timestamp1, timestamp2, timestamp3, ...],
   *      ...
   *   ]
   */
  public function getRequestCacheSummary() {
    $obj = $this->connection->select('netforum_request_cache', 'r')
      ->fields('r', ['request', 'timestamp', ])
      ->execute();
    if (empty($obj)) {
      return [];
    }
    $return = [];
    foreach ($obj as $row) {
      if (empty($return[$row->request])) {
        $return[$row->request] = [];
      }
      $return[$row->request][] = $row->timestamp;
    }
    return $return;
  }

  /**
   * Insert a request cache item.
   *
   * Inserts a new record or overwrites an existing record with a new timestamp.
   *
   * @param array $params
   *   An array with keys
   *    - user_name: the xWeb user name being used.
   *    - wsdl_loc: the location of the WSDL being used.
   *    - request: the netFORUM xWeb request. Example: 'GetQuery'.
   *    - arguments_sha1_hash: sha-1-encoded serialized request arguments.
   *        Created using
   *        <code>
   *          $arguments_sha1_hash = sha1(serialize($arguments));
   *        </code>
   *    - response: the serialized xWeb response.
   *
   * @return bool
   *   1 = success, 0 = failure.
   */
  public function insertRequestCacheItem($params) {
    // Add the timestamp of this response.
    $params['timestamp'] = time();
    $return_value = $this->connection->merge('netforum_request_cache')
      ->key(['timestamp' => $params['timestamp'],])
      ->fields($params)
      ->execute();
    return !empty($return_value);
  }

  /**
   * Prune the request cache of expired entries.
   *
   * @return int
   *   The number of pruned entries.
   */
  public function pruneRequestCache() {
    $settings = $this->settings->allCacheSettings();
    $count = 0;
    foreach ($settings as $request => $setting) {
      $duration = $this->convertDuration($setting['duration']);
      $min_timestamp = time() - $duration;
      $count += $this->connection->delete('netforum_request_cache')
        ->condition('request', $request)
        ->condition('timestamp', $min_timestamp, '<')
        ->execute();
    }
    return $count;
  }

  /**
   * Clear the request cache of all entries.
   *
   * @return int
   *   The number of deleted entries.
   */
  public function clearRequestCache() {
    return $this->connection->delete('netforum_request_cache')
      ->execute();
  }

  /**
   * Clear the request cache for one request type.
   *
   * @param string $request
   *   The netFORUM xWeb request. Examples: 'GetQuery', 'ExecuteMethod'.
   *
   * @return int
   *   The number of deleted entries.
   */
  public function clearOneRequestCache(string $request) {
    return $this->connection->delete('netforum_request_cache')
      ->condition('request', $request)
      ->execute();
  }

  /**
   * Prune the request cache of expired entries for on request type. 
   *
   * @param string $request
   *   The netFORUM xWeb request. Examples: 'GetQuery', 'ExecuteMethod'.
   *
   * @return int
   *   The number of pruned entries.
   */
  public function pruneOneRequestCache(string $request) {
    $duration = $this->settings->cacheDurationTime($request);
    $min_timestamp =  time() - $duration;
    return $this->connection->delete('netforum_request_cache')
      ->condition('request', $request)
      ->condition('timestamp', $min_timestamp, '<')
      ->execute();
  }

  /**
   * Utility function for converting a duration string into a number of
   * seconds.
   *
   * @param string
   *   A duration string suitable for strtotime(). Example: '2 hours'.
   *
   * @return int
   *   The duration string in seconds. Example: 120.
   */
  protected function convertDuration($duration) {
    $now = time();
    return strtotime("+$duration", $now) - $now;
  }

}
